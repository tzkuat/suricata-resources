function init (args)
    local needs = {}
    needs["tls"] = tostring(true)
    return needs
end

function match(args)
    version, subject, issuer, fingerprint = TlsGetCertInfo();

    if subject == issuer then
        --[[selfSigned = string.format("\nIssuer: %s\nSubject: %s\n are the same.\nSelf Signed Certificate!\n",
                        issuer, subject)
        print(string.format("\nIssuer: %s\nSubject: %s\nare the same.\nSelf Signed Certificate!\n",
                        issuer, subject))]]
        return 1
    else
        return 0
    end
    --[[str = string.format("\nVersion %s\nIssuer %s\nSubject %s\nFingerprint %s",
                        version, issuer, subject, fingerprint)
    SCLogInfo(str);]]
end
